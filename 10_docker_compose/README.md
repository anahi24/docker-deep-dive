
---

# Docker Compose

---
# Docker Compose

Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration

Using Compose is basically a three-step process:

- Define your app’s environment with a Dockerfile so it can be reproduced anywhere.
- Define the services that make up your app in docker-compose.yml so they can be run together in an isolated environment.
- Run **docker compose up** and the Docker compose command starts and runs your entire app. You can alternatively run docker-compose up using the docker-compose binary.

---

# Docker Compose (cont.)


Compose has commands for managing the whole lifecycle of your application:

- Start, stop, and rebuild services
- View the status of running services
- Stream the log output of running services
- Run a one-off command on a service


---

# Installing Docker Compose


Docker Compose relies on *Docker Engine* for any meaningful work, so make sure you have *Docker Engine* installed either locally or remote, depending on your setup.

- On desktop systems like Docker Desktop for Mac and Windows, Docker Compose is included as part of those desktop installs.
- On Linux systems, first install the Docker Engine for your OS as described earlier in the course, then run the next command to install *docker-compose*


```sh
bash~$ sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)"\
                 -o /usr/local/bin/docker-compose
bash~$ sudo chmod +x /usr/local/bin/docker-compose
bash~$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

the last step is optional, in case you don't have */usr/local/bin* in your *$PATH* variable.

---

# Practice

- Install docker compose from cli using latest repository at github

---
# Compose Commands

here is a short list of mainly used commands with *docker-compose*

- build: helps to build the image
- up: creates and starts the containers
- down: stop and remove containers, networks, images and volumes
- ps: list containers
- stop: stop the service
- start: starts the service
- restart: restarts the service
- scale:  

---

# Creating a Compose File

Typically, docker compose file is called **docker-compose.yaml**. one can give a different name , but in that case you'll have use *-f* flag to provide the path for the file to work.

Compose files are usually yaml files, although there is an option of writing and using it in json.

The classic docker-compose file usually consists of 4 top level keys:

- version: a mandatory key, that defines to which api of docker you need to communicate with.
- services: defines the list of containers that we're going to use. name of the service will the name of the actual container.
    - it is possible to do image builds with docker compose  by using *build* key  and pass to it your *context* of actual dockerfile and *args* for build.
- volumes: creates or mounts host paths or named volumes, specified as sub-options to a service.
- networks: creates and connects containers in CNM based networks,

---

# Creating a Compose File (cont.)

Additional top level keys that one might encounter would be:

- secrets: enables users to keep and manage sensitive data such as passwords and alike.
- configs: provides with possibility to apply custom configurations.

> `[!]`Note: We are NOT going to cover these mainly because these are main used in docker-swarm, a topic that we are also NOT going to cover.
  

---

# Using Volumes With Compose

## Overview

<img src="99_misc/.img/compose_sto_0.png" alt="drawing" style="float:left;width:360px;">
<img src="99_misc/.img/compose_sto_1.png" alt="drawing" style="float:right;width:360px;">

---

# Using Volumes With Compose

## Simple volumes

Simple volume to mount file to file as an configuration file

```yaml
version: "2"
services:
  web:
    image: "nginx:latest"
    volumes: 
      - ./default.conf:/etc/nginx/conf.d/default.conf
    ports:
      - 80:80
```

---

# Using Volumes With Compose

## Simple volumes (cont.)

It is also possible to mount local folder directly to container as well
```yaml
version: "2"
services:
  web:
    image: "nginx:latest"
    volumes: 
      - ./conf.d:/etc/nginx/conf.d/
    ports:
      - 80:80
```
---
# Using Volumes With Compose

## Simple volumes (cont.)

By default, the volumes have `read/write` permissions, yet we can specify them to be `read-only` by adding `ro`

```yaml
version: "2"
services:
  web:
    image: "nginx:latest"
    volumes: 
      - ./conf.d:/etc/nginx/conf.d/:ro
    ports:
      - 80:80
```

---

# Using Volumes With Compose

## Named volumes

In some applications, shared storage is a `must` requirement. Meaning that several parts of the same application may require to access same storage, and that is where   `named volumes` come in play:

```yaml
version: "2"
services:
  web:
    image: "nginx:latest"
    volumes: 
      - storage:/usr/share/nginx/html
volumes:
  storage:
```
We can specify storage under `volumes` and under it name of volume that will be created automatically.

---

# Using Volumes With Compose

## Named volumes (cont.)

```yaml
version: "2"
services:
  web:
    image: "nginx:latest"
    volumes: 
      - storage:/usr/share/nginx/html
  reporting:
    image: "nginx:latest"
    volumes_from: *
      - web:ro
volumes:
  storage:
```

---

# Using Volumes With Compose

## External volumes 

```yaml
version: "2"
services:
  web:
    image: "nginx:latest"
    volumes: 
      - storage:/usr/share/nginx/html
volumes:
  storage:
```

---

# Using Volumes With Compose

## Summary


---

# Practice


---

# Using Network With Compose

## Overview
<img src="99_misc/.img/compose_net_0.png" alt="separate networks" style="float:left;width:360px;">
<img src="99_misc/.img/compose_net_1.png" alt="default network" style="float:right;width:360px;">
<img src="99_misc/.img/compose_net_2.png" alt="isolated network" style="float:left;width:360px;">
<img src="99_misc/.img/compose_net_3.png" alt="multi-network" style="float:right;width:360px;">


---

# Using Network With Compose

## Default Network


---

# Using Network With Compose

## Isolating Containers


---

# Using Network With Compose

## Alias and Container Networks

---


# Using Network With Compose

## Links


